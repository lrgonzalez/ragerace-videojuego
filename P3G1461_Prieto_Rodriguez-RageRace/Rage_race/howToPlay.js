var learnState = {
    
    create: function() {
        
        // Imagen de fondo
        var image = game.add.image(0, 0, 'background');
        //image.scale.setTo(0.75, 0.6);
        image.height = game.height;
        image.width = game.width;
        image.smoothed = false;
        
        var bck = game.add.image(0, 0, 'grey_back');
        bck.position.x = game.world.centerX/2-180;
        bck.position.y = game.world.centerY/2;
        bck.scale.setTo(4.1, 1.8);
        bck.alpha = 0.8;
        

        var titulo = game.add.bitmapText(0,0, 'myfont', 'Rage Race', 40);
        titulo.position.x = 0;
        titulo.position.y = 60;
        titulo.anchor.setTo(0.5, 0.5);
        
        game.add.tween(titulo).to({x:game.global.worldWidth/2}, 1000, Phaser.Easing.Bounce.Out).start();
        
        
        //Instrucciones        
        var style = { font: 'bold 20pt NewCentury', fill: 'red', align: 'left', wordWrap: true, wordWrapWidth: 1000 };
        var texto = game.add.text(game.world.centerX, game.world.centerY-80, "Jugar a Rage Race es muy sencillo! Solo necesitas usar las flechas del teclado izquierda y derecha, o en caso de que tengas un pad, puedes conectarlo antes de iniciar el juego y usarlo para moverte.\nRecoge puntos, usa las ventajas y escapa antes de que se acabe el tiempo o tu gasolina!", style);
        texto.anchor.set(0.5);
        
        var style = { font: 'bold 20pt NewCentury', fill: '#f86207', align: 'center', wordWrap: true, wordWrapWidth: 1000 };
        var texto = game.add.text(game.world.centerX, game.world.centerY+15, "Usar->                              Esquivar->", style);
        texto.anchor.set(0.5);
        
        
        var style2 = { font: 'bold 20pt NewCentury', fill: 'black', align: 'left', wordWrap: true, wordWrapWidth: 1000 };
        var texto2 = game.add.text(game.world.centerX-200, game.world.centerY+130,"\nRECOLECTABLES\nBilletes: te darán puntos\nGasolina: si te quedas sin ella se acabará el juego!\nLlamas: quema a tus enemigos!\nCopos de nieve: reduce su velocidad\nEscudo: invulnerabilidad durante unos segundos\nTiempo extra: +5 segundos", style2);
        texto2.anchor.set(0.5);
        
        //Sprites
        var flechas = this.add.sprite(game.world.centerX-40, game.world.centerY+25, 'key_flechas');
        flechas.scale.setTo(0.5, 0.5);
        flechas.anchor.set(0.5);
        var enemigo = this.add.sprite(game.world.centerX+250, game.world.centerY, 'enemigo');
        enemigo.scale.setTo(0.5, 0.5);
        
        var billete = this.add.sprite(game.world.centerX+170, game.world.centerY+50, 'billete', 0);
        billete.scale.setTo(1.2, 1.2);
        var gasolina = this.add.sprite(game.world.centerX+100, game.world.centerY+85, 'bidon');
        gasolina.scale.setTo(0.4, 0.4);
        var fuego = this.add.sprite(game.world.centerX+170, game.world.centerY+110, 'icono-fuego');
        fuego.scale.setTo(0.1, 0.1);
        var copo = this.add.sprite(game.world.centerX+100, game.world.centerY+160, 'copo');
        copo.scale.setTo(0.8, 0.8);
        var escudo = this.add.sprite(game.world.centerX+170, game.world.centerY+200, 'shield_icon');
        escudo.scale.setTo(0.4, 0.4);  
        var tiempo = this.add.sprite(game.world.centerX+100, game.world.centerY+230, 'time_icon');    
        tiempo.scale.setTo(0.8, 0.8);
        
        //Boton volver al menu
        //game.add.text(80, 600, "Cancelar", {font: "30px Arial", fill: "#ffffff"}).anchor.set(0.5);
        var volverMenu = game.add.button(10, 580, 'boton_volver_menu', this.volverClicked, this, 2, 1, 0);   
    },
    
    volverClicked: function(){
        game.state.start("menu");
    },
    
}