var bootState = {
    preload: function () {
        game.load.image('progressBar', 'assets/progressBar.png');
    },

    create: function() {
        game.stage.backgroundColor = '#000000';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        // Empieza en load
        game.state.start('load');
    }
};