var selectState = {
    
    create: function() {
        
        // Imagen de fondo
        var image = game.add.image(0, 0, 'background');
        //image.scale.setTo(0.75, 0.6);
        image.height = game.height;
        image.width = game.width;
        image.smoothed = false;
        

        var titulo = game.add.bitmapText(0,0, 'myfont', 'Rage Race', 80);
        titulo.position.x = 0;
        titulo.position.y = 80;
        titulo.anchor.setTo(0.5, 0.5);
        
        game.add.tween(titulo).to({x:game.global.worldWidth/2}, 1000, Phaser.Easing.Bounce.Out).start();
        
        
        // BOTÓN DE MUTE (SONIDO ON/OFF)
        this.muteButton = game.add.button(20, 20, 'mute', this.toggleSound, this);
        // Si esta el cursor encima
        this.muteButton.input.useHandCursor = true;
        
        // If the game is already muted
        if (game.sound.mute) {
            // Change the frame to display the speaker with no sound
            this.muteButton.frame = 1;
        }
        
        
        //Coches disponibles
        var colors = ['car','car2','car3','car4','car5','car6','car7'];
        
        this.scrollingMap = game.add.tileSprite(0, 0, game.width + colors.length * 190, game.height, "transp");
        this.scrollingMap.inputEnabled = true;
        this.scrollingMap.input.enableDrag(false);
        this.scrollingMap.savedPosition = new Phaser.Point(this.scrollingMap.x, this.scrollingMap.y);
        this.scrollingMap.isBeingDragged = false; 
        this.scrollingMap.movingSpeed = 0; 
        this.scrollingMap.input.allowVerticalDrag = false;
        this.scrollingMap.input.boundsRect = new Phaser.Rectangle(game.width - this.scrollingMap.width, game.height - this.scrollingMap.height, this.scrollingMap.width * 2 - game.width, this.scrollingMap.height * 2 - game.height);
        
        console.log('game.global.worldHeight', game.global.worldHeight);
        console.log('game.heigh', game.width);
        
        for(var i = 0; i < colors.length; i++){
            var car = game.add.image(game.width / 2 + i * 220, game.height / 2, colors[i]);
            car.anchor.set(0.5);
            this.scrollingMap.addChild(car)
        }
        this.scrollingMap.events.onDragStart.add(function(){
            this.scrollingMap.isBeingDragged = true;
            this.scrollingMap.movingSpeed = 0;
        }, this);
        this.scrollingMap.events.onDragStop.add(function(){
            this.scrollingMap.isBeingDragged = false;
        }, this);
    
        //Boton cambio coche
        var cambioCoche = game.add.button((game.global.worldWidth-190)/2, 450, 'seleccionar', this.cambioClick, this, 2, 1, 0);
        
        //Boton volver al menu
        var volverMenu = game.add.button(10, 500, 'boton_volver_menu', this.volverClicked, this, 2, 1, 0);        
        
    },
    update:function(){
        var speedMult = 0.7;
        var friction = 0.99;
        
        var zoomed = false;
        for(var i = 0; i < this.scrollingMap.children.length; i++){
            if(Math.abs(this.scrollingMap.children[i].world.x - game.width / 2) < 46 && !zoomed){
                this.scrollingMap.getChildAt(i).scale.setTo(1.5);
                this.scrollingMap.children[i].zoomed = true;
                zoomed = true;
            }
            else{
                this.scrollingMap.getChildAt(i).scale.setTo(1);   
                this.scrollingMap.children[i].zoomed = false;
            }
        }
        if(this.scrollingMap.isBeingDragged){
            this.scrollingMap.savedPosition = new Phaser.Point(this.scrollingMap.x, this.scrollingMap.y);
        }
        else{
            if(this.scrollingMap.movingSpeed > 1){
                this.scrollingMap.x += this.scrollingMap.movingSpeed * Math.cos(this.scrollingMap.movingangle);
                if(this.scrollingMap.x < game.width - this.scrollingMap.width){
                    this.scrollingMap.x = game.width - this.scrollingMap.width;
                    this.scrollingMap.movingSpeed *= 0.5;
                    this.scrollingMap.movingangle += Math.PI;

                }
                if(this.scrollingMap.x > 0){
                    this.scrollingMap.x = 0;
                    this.scrollingMap.movingSpeed *= 0.5;
                    this.scrollingMap.movingangle += Math.PI;
                }
                this.scrollingMap.movingSpeed *= friction;
                this.scrollingMap.savedPosition = new Phaser.Point(this.scrollingMap.x, this.scrollingMap.y);
            }
            else{
                var distance = this.scrollingMap.savedPosition.distance(this.scrollingMap.position);
                var angle = this.scrollingMap.savedPosition.angle(this.scrollingMap.position);
                if(distance > 4){
                    this.scrollingMap.movingSpeed = distance * speedMult;
                    this.scrollingMap.movingangle = angle;
                }                
            }
        }
    },
    
    cambioClick: function(){       
        //choiseCar = game.add.text(game.width / 2, 400, 'Elige tu coche!', {font: "18px Arial", fill: '#fff', align: 'center' });
        //choiseCar.anchor.setTo(0.5, 0.5);
        for(var i = 0; i < this.scrollingMap.children.length; i++){
            if(this.scrollingMap.children[i].zoomed == true){
                //Se establece el seleccionado como nuevo coche del usuario y se vuelve al menu
                game.global.car = this.scrollingMap.children[i].key;
                game.state.start("menu");                
            }            
        }        
    },
    volverClicked: function(){
        game.state.start("menu");
    },
    
    // FUNCIÓN PARA MUTE/UNMUTE
    toggleSound: function() {
        // Switch the Phaser sound variable from true to false, or false to true
        // When 'game.sound.mute = true', Phaser will mute the game
        game.sound.mute = ! game.sound.mute;
        // Change the frame of the button
        this.muteButton.frame = game.sound.mute ? 1 : 0;       
    },    
};