var loadState = {
    preload: function () {        
        game.time.advancedTiming = true;
        
        var loadingLabel = game.add.text(game.world.centerX, 150, 'loading...',{ font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        
        // Barra de carga
        var progressBar = game.add.sprite(game.world.centerX, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        
        // Cargamos nuestros assets
        // Fuentes de textos
        game.load.bitmapFont('myfont', 'assets/font/font.png', 'assets/font/font.fnt'); //fuente descargada para titulo
        game.load.bitmapFont('fontTime', 'assets/fontTime/fontTime.png', 'assets/fontTime/fontTime.fnt'); //fuente descargada para tiempo
        game.load.bitmapFont('fontScore', 'assets/fontScore/font.png', 'assets/fontScore/font.fnt'); //fuente descargada para puntuacion del menu
        game.load.bitmapFont('fontGameOver_score', 'assets/fontGameOver_score/font.png', 'assets/fontGameOver_score/font.fnt'); //fuente fin
        game.load.bitmapFont('fontH2P', 'assets/fontH2P/font.png', 'assets/fontH2P/font.fnt');  //fuente how to play
        
        // Fondo del MENU INICIAL
        game.load.image('background', 'assets/title.jpg');
        //icono mute
        game.load.spritesheet('mute', 'assets/muteButton.png', 28, 22);    
        //Niveles
        game.load.spritesheet("levels", "assets/levels.png", game.global.thumbWidth, game.global.thumbHeight);  
        
        //fondo INSTRUCCIONES
        game.load.image('grey_back', 'assets/fondo_gris.jpg');
        //boton how to play
        game.load.image("instrucciones", "assets/Help.png");
        game.load.image("key_flechas", "assets/flechas.png");
        
        //boton cambio coche
        game.load.spritesheet('cambio_coche', 'assets/coche.png', 193, 71);
        //boton seleccionar
        game.load.spritesheet('seleccionar', 'assets/seleccionar.png', 193, 71); 
        //boton volver al menu
        game.load.spritesheet('boton_volver_menu', 'assets/volver.png', 193, 71);
        game.load.image("transp", "assets/transp.png");
        
        //Botones de menu de pausa
        game.load.image('menu', 'assets/pause_menu2.png', 628, 558);
        
        //Modelos de coches elegir
        game.load.image('car', 'assets/blue_car.png');
        game.load.image('car2', 'assets/green_car.png');
        game.load.image('car3', 'assets/orange_car.png');
        game.load.image('car4', 'assets/pink_car.png');
        game.load.image('car5', 'assets/red_car.png');
        game.load.image('car6', 'assets/white_car.png');
        game.load.image('car7', 'assets/yellow_car.png');
        
        game.load.image('enemigo', 'assets/police_black - copia.png');
        game.load.image('enemigo_congelado', 'assets/enemigo_congelado.png');
        
        //de los niveles tiled
        game.load.image('dark_grass', 'assets/dark_grass.png');
        game.load.image('light_grass', 'assets/light_grass.png');
        game.load.image('light_sand', 'assets/light_sand.png');
        game.load.image('light_sand2', 'assets/light_sand2.png');
        game.load.image('fondo_hielo', 'assets/fondo_hielo.png');
        game.load.image('carretera', 'assets/carretera.jpg');
        game.load.image('neumatico', 'assets/neumatico.png');
        game.load.image('snow_texture', 'assets/snow_texture.png');
        game.load.image('arbol1-192x160', 'assets/arbol1-192x160.png');
        game.load.image('arbol2-480x384', 'assets/arbol2-480x384.png');
        game.load.image('arbol2-192x160', 'assets/arbol2-192x160.png');
        game.load.image('snowman64x64', 'assets/snowman64x64.png');
        game.load.image('snowman2-64x64', 'assets/snowman2-64x64.png');
        game.load.image('snowman3-64x64', 'assets/snowman3-64x64.png');
        game.load.image('snowman4-64x64', 'assets/snowman4-64x64.png');
        
        game.load.tilemap('level1', 'assets/mapa1.json', null, Phaser.Tilemap.TILED_JSON); //mapa carretera
        game.load.tilemap('level2', 'assets/mapa2.json', null, Phaser.Tilemap.TILED_JSON); //mapa bosque
        game.load.tilemap('level3', 'assets/mapa3.json', null, Phaser.Tilemap.TILED_JSON); //mapa nieve
        
        
        game.load.image('barro', 'assets/earth.png');
        game.load.image('arbol', 'assets/tree.png');
        game.load.image('flor', 'assets/flower.png');
        game.load.image('fuente', 'assets/fountain2.png');
        game.load.image('copo', 'assets/snowflake cartoon.png');
        game.load.image('bidon', 'assets/gasoline.png');
        game.load.spritesheet('billete', 'assets/billetes.png', 57.14, 29, 7);
        game.load.image('icono-fuego', 'assets/fire.png');
        game.load.spritesheet('shield_animation', 'assets/shield_animation.png', 192, 192, 20); 
        game.load.image('shield_icon', 'assets/escudo_peq.png');
        game.load.image('time_icon', 'assets/time_icon.png');
        
        //nieve
        game.load.spritesheet('snowflakes', 'assets/snowflakes.png', 17, 17);
        game.load.spritesheet('snowflakes_large', 'assets/snowflakes_large.png', 64, 64);
        game.load.image('placa_hielo', 'assets/hockey_ice.png');
        
        //Emitter fuego
        game.load.image('fire1', 'assets/fire1.png');
        game.load.image('fire2', 'assets/fire2.png');
        game.load.image('fire3', 'assets/fire3.png');
        game.load.image('smoke', 'assets/smoke-puff.png');
        
        //Emitter de muerte de enemigos
        //game.load.image('pixel', 'assets/pixel.png');
        game.load.spritesheet('kaboom', 'assets/explosion.png', 64, 64, 23);
        

        //SONIDOS
        game.load.audio('point_sound', 'assets/sounds/mario_coin.mp3'); //sonido recoger moneda
        game.load.audio('back_music', 'assets/sounds/backMusic.mp3'); //musica de fondo
        game.load.audio('bomb_sound', 'assets/sounds/Bomb_sound.mp3'); //explosion de enemigo
        game.load.audio('blop', 'assets/sounds/blop.mp3'); //sonido recoger bidon
        game.load.audio('fire_sound', 'assets/sounds/lanzallamas-corto.mp3'); //sonido recoger bidon
        game.load.audio('time_sound', 'assets/sounds/time_sound.mp3');
        game.load.audio('ice_sound', 'assets/sounds/Freeze spell.mp3');
        game.load.audio('shield_sound', 'assets/sounds/HUD_sound.mp3');
        game.load.audio('victory_sound', 'assets/sounds/victory.mp3');
        game.load.audio('collision_sound', 'assets/sounds/golpe-boing.mp3');  
    },
    
    create: function() {
        // Va al menu
        game.state.start('menu');
    }
};