// Inicializa Phaser
var game = new Phaser.Game((window.screen.availWidth * window.devicePixelRatio)-20, (window.screen.availHeight * window.devicePixelRatio)-80, Phaser.CANVAS, 'game');

// Variables globales
game.global = {
    score: 0,
    worldWidth: (window.screen.availWidth * window.devicePixelRatio)-20,
    worldHeight: (window.screen.availHeight * window.devicePixelRatio)-80,
    
    //Para los niveles
    // filas
    thumbRows : 1,
	// columnas
	thumbCols : 3,
	// width of a thumbnail, in pixels
	thumbWidth : 64,
	// height of a thumbnail, in pixels
	thumbHeight : 64,
	// space among thumbnails, in pixels
	thumbSpacing : 8,
	// array with finished levels and stars collected.
	// 0 = playable yet unfinished level
	// 1, 2, 3 = level finished with 1, 2, 3 stars
	// 4 = locked
    //Empezaremos con solo 3 niveles disponibles
	starsArray : [0,4,3],
	// level currently playing
	level : 0,
    
    //Coche del jugador (por defecto azul)
    car: 'car'
};


// Añadimos los estados
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('carSelect', selectState);
game.state.add('howToPlay', learnState);
game.state.add('level1', playState);
game.state.add('level2', playState2);
game.state.add('level3', playState3);
// Empieza en boot
game.state.start('boot');