var menuState = {
    
    create: function() {
        
        // Para reestablecer el tamaño de la pantalla
        game.world.setBounds(0,0,game.width, game.height);
        // Imagen de fondo
        var image = game.add.image(0, 0, 'background');
        image.height = game.height;
        image.width = game.width;
        image.smoothed = false;

        
        var titulo = game.add.bitmapText(0,0, 'myfont', 'Rage Race', 80);
        titulo.position.x = 0;
        titulo.position.y = 80;
        titulo.anchor.setTo(0.5, 0.5);
        
        game.add.tween(titulo).to({x:game.global.worldWidth/2}, 1000, Phaser.Easing.Bounce.Out).start();
        

        //Para mejor puntuacion
        if (!localStorage.getItem('bestScore')) { //primera vez que se ejecuta
            localStorage.setItem('bestScore', 0);
        }
        
        if (game.global.score > localStorage.getItem('bestScore')) {
            // Actualizacion mejor puntuacion
            localStorage.setItem('bestScore', game.global.score);
        }

        var scoreLabel = game.add.bitmapText(0,0, 'fontScore', 'ULTIMA PUNTUACION ALCANZADA: ' + game.global.score + '\nMEJOR PUNTUACION: ' + localStorage.getItem('bestScore'), 50);
        scoreLabel.position.x = game.world.centerX;
        scoreLabel.position.y = game.world.centerY-20;
        scoreLabel.align='center';
        scoreLabel.anchor.setTo(0.5, 0.5);
        
        
        // BOTÓN DE MUTE (SONIDO ON/OFF)
        this.muteButton = game.add.button(20, 20, 'mute', this.toggleSound, this);
        // Si esta el cursor encima
        this.muteButton.input.useHandCursor = true;
        
        // Si el juego y aesta muteado
        if (game.sound.mute) {
            // Se pone el frame del boton de muteo que corresponde
            this.muteButton.frame = 1;
        }
        
        //Para guardar los niveles
        if (!localStorage.getItem('starsArrayCad')) { //primera vez que se ejecuta
            localStorage.setItem('starsArrayCad', game.global.starsArray);
        }
        starsArrayCad = localStorage.getItem('starsArrayCad');
        var starsArray = []
        for (var i = 0; i < starsArrayCad.length ; i++){
            if (starsArrayCad[i] != ',')
                starsArray.push(parseInt(starsArrayCad[i]))
        }
        
        //Recorrer el stars array buscando si es diferente el global al de local storage
        for(var i = 0; i < game.global.thumbRows; i ++){
            for(var j = 0; j < game.global.thumbCols; j ++){
                var levelNumber = i*game.global.thumbCols+j;
                // si el antiguo tenia menos estrellas o estaba bloqueado, actualizar
                if (starsArray[levelNumber] != 4 && game.global.starsArray[levelNumber] < starsArray[levelNumber] || game.global.starsArray[levelNumber] == 4){
                    game.global.starsArray[levelNumber] = starsArray[levelNumber];
                // si no, actualizar local
                }else if (starsArray[levelNumber] == 4 || game.global.starsArray[levelNumber] > starsArray[levelNumber] && game.global.starsArray[levelNumber] != 4){
                    localStorage.setItem('starsArrayCad', game.global.starsArray);
                }
            }
        }
 
        //Para niveles
        // grupo de niveles
		var levelThumbsGroup = game.add.group();
        // determinar ancho y alto de los niveles por pagina (nosotros solo tenemos una pagina de niveles)
		var levelLength = game.global.thumbWidth*game.global.thumbCols+game.global.thumbSpacing*(game.global.thumbCols-1);
		var levelHeight = game.global.thumbWidth*game.global.thumbRows+game.global.thumbSpacing*(game.global.thumbRows-1);
        
		// horizontal offset to have level thumbnails horizontally centered in the page
        //var offsetX = (game.width-levelLength)/2+game.width;
        var offsetX = (game.global.worldWidth-levelLength)/2;
        // I am not interested in having level thumbnails vertically centered in the page, but
        // if you are, simple replace my "20" with
        // (game.height-levelHeight)/2
        var offsetY = 400;
        // looping through each level thumbnails
         for(var i = 0; i < game.global.thumbRows; i ++){
            for(var j = 0; j < game.global.thumbCols; j ++){  
                // which level does the thumbnail refer?
                var levelNumber = i*game.global.thumbCols+j;
                // adding the thumbnail, as a button which will call thumbClicked function if clicked   		
                var levelThumb = game.add.button(offsetX+j*(game.global.thumbWidth+game.global.thumbSpacing), offsetY+i*(game.global.thumbHeight+game.global.thumbSpacing), "levels", this.thumbClicked, this);	
                // Mostrar las estrellitas adecuadas
                levelThumb.frame=game.global.starsArray[levelNumber];
                // custom attribute (mostrar el numero del nivel)
                levelThumb.levelNumber = levelNumber+1;
                // adding the level thumb to the group
                levelThumbsGroup.add(levelThumb);
                // if the level is playable, also write level number
                if(game.global.starsArray[levelNumber]<4){
                    var style = {
                        font: "18px Arial",
                        fill: "#ffffff"
                    };
                    var levelText = game.add.text(levelThumb.x+5,levelThumb.y+5,levelThumb.levelNumber,style);
                    levelText.setShadow(2, 2, 'rgba(0,0,0,0.5)', 1);
                    levelThumbsGroup.add(levelText);
                }
            }
        }
        //Boton cambio coche
        var cambioCoche = game.add.button((game.global.worldWidth-190)/2, 500, 'cambio_coche', this.cambioClick, this, 2, 1, 0);   
        
        //Boton instrucciones como jugar
        var instrucciones = game.add.button(20, 500, 'instrucciones', this.instruccionesClick, this);
        
    },
    
    cambioClick: function(){
        game.state.start("carSelect");
    },
    
    instruccionesClick: function(){
        game.state.start("howToPlay");
    },
    
    
    //Cuando clicamos en un nivel
    thumbClicked:function(button){
		// Si el nivel está disponible, lo jugamos
		if(button.frame < 4){
			game.global.level = button.levelNumber;
            // Distintos niveles
            if (game.global.level == 1)
                game.state.start("level1");
            else if(game.global.level == 2)
                game.state.start("level2");
            else if(game.global.level == 3)
                game.state.start("level3");
		}
		// Si no, animación de "agitar" nivel bloqueado
		else{
			var buttonTween = game.add.tween(button)
			buttonTween.to({
				alpha: 0.5
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.to({
				alpha: 1
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.to({
				alpha: 0.5
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.to({
				alpha: 1
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.start();
		}
	},
    
    // FUNCIÓN PARA MUTE/UNMUTE
    toggleSound: function() {
        // Si esta muteado desmutea y viceversa
        game.sound.mute = ! game.sound.mute;
        // Y se cambia el frame del boton
        this.muteButton.frame = game.sound.mute ? 1 : 0;       
    },
    
};