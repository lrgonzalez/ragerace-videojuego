CocheEnemigo = function (game, player) {
    var x = game.world.randomX;
    var y = game.world.randomY;

    this.game = game;
    this.player = player;
    this.alive = true;
    
    // sprite, nombre
    this.enemigo = game.add.sprite(x, y, 'enemigo');
    this.colisionando = false;

    this.enemigo.anchor.set(0.5); //importante para colision
    this.enemigo.scale.setTo(0.5, 0.5);
    
    game.physics.enable(this.enemigo, Phaser.Physics.ARCADE);
    this.enemigo.body.immovable = false;
    this.enemigo.body.collideWorldBounds = true;
    this.enemigo.body.bounce.setTo(1, 1);

    this.enemigo.angle = game.rnd.angle();

    game.physics.arcade.velocityFromRotation(this.enemigo.rotation, 200, this.enemigo.body.velocity);

};

CocheEnemigo.prototype.update = function(){
    // para que la rotacion vaya cambiando
    this.enemigo.rotation = this.game.physics.arcade.angleBetween(this.enemigo, this.player);
    
    if (this.game.physics.arcade.distanceBetween(this.enemigo, this.player) < 430){ //solo se acercan cuando estamos cerca
        this.game.physics.arcade.moveToObject(this.enemigo, this.player, 200); //velocidad a la que se acercan a por el jugador
    }
}

var playState2 = {    
    create: function() {
        TIEMPO_BONUS = 6000 //6 secs para que aparezca un bonus
        START_X = 580 //Coordenadas de salida inicial
        START_Y = 480
        ENEMIGOS_MAX = 7;
        
        counter = 60; //Tiempo de juego inicial
        
        game.physics.startSystem(Phaser.Physics.ARCADE);

        //  Tamaño del munodo como cuadrado de 2000 x 2000
        game.world.setBounds(-1000, -1000, 2000, 2000);

        bg = game.add.tileSprite(0, 0, 800, 600, 'background');
        bg.fixedToCamera = true;

        //TILEMAP
        land = game.add.tilemap('level2');

        land.addTilesetImage('dark_grass');
        land.addTilesetImage('light_grass');
        land.addTilesetImage('light_sand2');        

        hierba = land.createLayer('Hierba');   //base
        camino = land.createLayer('Camino');
        arbustos = land.createLayer('Arbustos');
        
        land.setLayer(arbustos); //seleccionar layer a definir
        //land.setCollision([]); //choca con...
        land.setCollisionByExclusion([]); //choca con todo salvo con...

        hierba.resizeWorld();
        
        // DECORACIONES
        flower = game.add.group();
        flower.enableBody = true;
        
        flor = flower.create(240, 140, 'flor');
        flor = flower.create(370, 300, 'flor');
        flor = flower.create(200, 430, 'flor');
        flor = flower.create(300, 600, 'flor');
        flor = flower.create(350, 780, 'flor');
        flor = flower.create(550, 940, 'flor');
        flor = flower.create(680, 680, 'flor');
        flor = flower.create(320, 1720, 'flor');
        flor = flower.create(510, 1010, 'flor');
        flor = flower.create(470, 1390, 'flor');
        flor = flower.create(200, 1430, 'flor');
        flor = flower.create(300, 1246 , 'flor');
        
        flor = flower.create(800, 100, 'flor');
        flor = flower.create(1300, 250, 'flor');
        flor = flower.create(1500, 80, 'flor');
        
        flor = flower.create(1800, 110, 'flor');
        flor = flower.create(2100, 230, 'flor');
        flor = flower.create(2300, 180, 'flor');
        
        flor = flower.create(2600, 220, 'flor');
        flor = flower.create(2800, 330, 'flor');
        flor = flower.create(3000, 700, 'flor');
        
        flor = flower.create(3100, 730, 'flor');
        flor = flower.create(2900, 860, 'flor');
        
        flor = flower.create(3000, 1030, 'flor');
        flor = flower.create(2800, 1100, 'flor');
        
        flor = flower.create(2800, 1430, 'flor');
        flor = flower.create(2700, 1600, 'flor');
        
        flor = flower.create(2750, 1830, 'flor');
        flor = flower.create(2700, 2000, 'flor');
        
        flor = flower.create(2800, 2230, 'flor');
        flor = flower.create(2900, 2500, 'flor');
        
        flor = flower.create(3050, 2730, 'flor');
        flor = flower.create(2800, 2900, 'flor');
        
        flor = flower.create(2650, 2930, 'flor');
        flor = flower.create(2400, 3100, 'flor');
        
        flor = flower.create(2150, 3030, 'flor');
        flor = flower.create(2400, 3100, 'flor');
        
        flor = flower.create(1950, 2930, 'flor');
        flor = flower.create(1700, 2800, 'flor');
        
        flor = flower.create(1550, 3000, 'flor');
        
        flor = flower.create(1450, 2630, 'flor');
        flor = flower.create(1500, 2900, 'flor');
        
        flor = flower.create(1250, 3000, 'flor');
        
        flor = flower.create(1050, 2900, 'flor');
        flor = flower.create(850, 3100, 'flor');
        flor = flower.create(750, 2900, 'flor');
        flor = flower.create(550, 2700, 'flor');
        
        flor = flower.create(1200, 1700, 'flor');
        flor = flower.create(1700, 1900, 'flor');
        flor = flower.create(1900, 1300, 'flor');
        flor = flower.create(1800, 1200, 'flor');
        
        flor = flower.create(1600, 1400, 'flor');
        flor = flower.create(1500, 1500, 'flor');
        flor = flower.create(1400, 1600, 'flor');
        flor = flower.create(1300, 1100, 'flor');
        
        fountain = game.add.group();
        fountain.enableBody = true;
        
        fuente = fountain.create(1500, 1300, 'fuente');
        fuente.body.immovable = true;
        
        fuente.scale.setTo(0.7, 0.7);
        
        //BARRIZALES
        barrizales = game.add.group();
        barrizales.enableBody = true;
        
        //crear charcos de barro individuales
        //cruce abajo izq
        charco = barrizales.create(600, 2200, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600+128, 2200, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600+128, 2200+128, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600, 2200+128, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600+256, 2200+128, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600+256, 2200, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600+384, 2200+128, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(600+384, 2200, 'barro');
        charco.body.immovable = true;
        
        //Cruce abajo dcha
        charco = barrizales.create(2300, 2300, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(2300, 2300-128, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(2300-128, 2300, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(2300-128, 2300-128, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(2300-128, 2300+128, 'barro');
        charco.body.immovable = true;
        
        //cruce arriba derecha
        charco = barrizales.create(2380, 710, 'barro');
        charco.body.immovable = true;
        
        charco = barrizales.create(2380-128, 710, 'barro');
        charco.body.immovable = true;
        
        //arriba izq        
        charco = barrizales.create(740, 600, 'barro');
        charco.body.immovable = true;
        
        
        //ARBOLES
        tree = game.add.group();
        tree.enableBody = true;
        
        arbol = tree.create(0, 0, 'arbol');
        arbol = tree.create(0, 100, 'arbol');
        arbol = tree.create(0, 300, 'arbol');
        arbol = tree.create(0, 500, 'arbol');
        arbol = tree.create(0, 700, 'arbol');
        arbol = tree.create(0, 900, 'arbol');
        arbol = tree.create(0, 1100, 'arbol');
        arbol = tree.create(0, 1300, 'arbol');
        arbol = tree.create(0, 1500, 'arbol');
        arbol = tree.create(0, 1700, 'arbol');
        arbol = tree.create(0, 1900, 'arbol');
        arbol = tree.create(200, 2000, 'arbol');
        arbol = tree.create(650, 1700, 'arbol');
        arbol = tree.create(600, 1500, 'arbol');
        arbol = tree.create(550, 1300, 'arbol');
        arbol = tree.create(500, 1100, 'arbol');
        arbol = tree.create(400, 700, 'arbol');
        
        arbol = tree.create(1400, 250, 'arbol');
        arbol = tree.create(1600, 300, 'arbol');
        arbol = tree.create(1800, 300, 'arbol');
        arbol = tree.create(2100, 400, 'arbol');
        
        arbol = tree.create(1200, 650, 'arbol');
        arbol = tree.create(1500, 700, 'arbol');
        arbol = tree.create(1700, 750, 'arbol');
        
        arbol = tree.create(900, 1000, 'arbol');
        arbol = tree.create(1000, 1300, 'arbol');
        
        arbol = tree.create(1100, 1900, 'arbol');
        arbol = tree.create(1500, 1900, 'arbol');
        arbol = tree.create(1900, 2000, 'arbol');
        arbol = tree.create(2000, 1400, 'arbol');
        
        arbol = tree.create(3000, 1100, 'arbol');
        arbol = tree.create(2900, 1300, 'arbol');
        arbol = tree.create(2800, 1500, 'arbol');
        
        arbol = tree.create(2800, 1700, 'arbol');
        arbol = tree.create(2900, 1900, 'arbol');
        arbol = tree.create(3000, 2100, 'arbol');
        arbol = tree.create(3000, 2100, 'arbol');
        
        arbol = tree.create(2600, 2500, 'arbol');
        
        arbol = tree.create(2900, 3000, 'arbol');
        arbol = tree.create(3000, 2900, 'arbol');
        
        arbol = tree.create(2200, 2700, 'arbol');
        arbol = tree.create(2000, 2600, 'arbol');
        arbol = tree.create(1700, 2500, 'arbol');
        arbol = tree.create(2600, 2500, 'arbol');
        arbol = tree.create(2600, 2500, 'arbol');
        
        arbol = tree.create(0, 2600, 'arbol');
        arbol = tree.create(200, 2700, 'arbol');
        arbol = tree.create(100, 2900, 'arbol');
        arbol = tree.create(600, 2500, 'arbol');
        arbol = tree.create(800, 2700, 'arbol');
        
        arbol = tree.create(2400, 0, 'arbol');
        arbol = tree.create(2700, 100, 'arbol');
        arbol = tree.create(3000, 300, 'arbol');
        //////////
        
        /////
        // FUEGO ANIMACION
        //400 particulas
        emitter = game.add.emitter(game.world.centerX, game.world.centerY, 400);
        //parametros: keys, frames, quantity, collide, collideWorldBounds
        emitter.makeParticles([ 'fire1', 'fire2', 'fire3', 'smoke' ], 0, this.maxParticles, true, true);
        
        emitter.setAlpha(1, 0, 3000); //algo de la transparencia del sprite
        emitter.setScale(0.8, 0, 0.8, 0, 3000);
        
        //// explode, lifespan, frequency, quantity
        emitter.start(false, 300, 5);
        emitter.on = false;
        
        flag_gameOver = false;

        // JUGADOR
        player = this.add.sprite(START_X, START_Y, game.global.car);
        player.name = 'player';
        player.debug = true;
        
        player.anchor.set(0.5);

        player.scale.setTo(0.5, 0.5);

        game.physics.arcade.enable(player);

        player.body.collideWorldBounds = true;
        
        player.body.bounce.setTo(1, 1);
        
        player.body.allowRotation = true;
        player.body.immovable = false;
        player.colisionando = false;
        
        player.invencible = false;
        player.invencibleBonus = false;
        
        //Escudo de energia invencibilidad        
        shield = game.add.sprite(START_X, START_X, 'shield_animation');
        shield.animations.add('brillar');
        shield.animations.play('brillar', 30, true);
        
        shield.anchor.setTo(0.5, 0.45);
        shield.scale.setTo(0.8, 0.8);
        
        shield.alpha = 0.6;
        
        // ENEMIGOS
        enemigos = [];
        enemigosTotal = ENEMIGOS_MAX;
        
        for (var i = 0; i < enemigosTotal; i++){
            enemigos.push(new CocheEnemigo(game, player));
        }
        flag_congelados = false;
        /////
        
        //ANIMACION DESTRUCCION COCHE ENEMIGO
        explosions = game.add.group();
        
        for (var i = 0; i < 10; i++){
            emitterDestruction = explosions.create(0, 0, 'kaboom', [0], false);
            emitterDestruction.anchor.setTo(0.5, 0.5);
            emitterDestruction.animations.add('kaboom');
        }
        ////////
        
        //Camara
        game.camera.follow(player);
        game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
        game.camera.focusOnXY(0, 0);

        //CURSORES
        cursors = this.input.keyboard.createCursorKeys();
        
        //PARA MENU DE PAUSA
        pauseKey = this.input.keyboard.addKey(Phaser.KeyCode.P); //P para pause
        pauseKey.onDown.add(pauseMenu, this);
        function pauseMenu(){
            //solo si no estaba ya pausado
            if (game.paused != true){               
                // pausar juego
                game.paused = true;

                // Menu
                menu = game.add.sprite(game.global.worldWidth/2, game.global.worldHeight/2, 'menu');

                //fixed to camera hecho a mano
                menu.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2) / this.game.camera.scale.x;
                menu.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2) / this.game.camera.scale.y;
                menu.anchor.setTo(0.5, 0.5);
                menu.scale.setTo(0.5, 0.5);
            }
        }        
        
        // Listener para el menu
        game.input.onDown.add(unpause, this);
        
        // Handler de pausa
        function unpause(event){
            if(game.paused && !flag_gameOver){
                // Calculate the corners of the menu
                var x1 = game.global.worldWidth/2 - 628/4, x2 = game.global.worldWidth/2 + 628/4,
                    y1 = game.global.worldHeight/2 - 558/4, y2 = game.global.worldHeight/2 + 558/4; //se divide entre 4 porque el tam del menu es la mitad

                // Si ha clicado dentro del menu
                if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
                    // Coordenadas locales del menu (donde hemos pulsado)
                    var x = (event.x - x1)*2, //se multiplica par arecuperar el tamaño original que antes hemos quitado
                        y = (event.y - y1)*2;
                    
                    // Coordenadas posibles
                    if (x > 75 && x < 211 && y < 392 && y > 195){
                        choise = 1;
                    } else if (x > 421 && x < 562 && y < 392 && y > 195){
                        choise = 0;
                    }else if (x > 110 && x < 520 && y < 486 && y > 380){
                        choise = 2;
                    }else choise = 3; //no hace nada

                    // Eleccion
                    if (choise == 0){ //salir
                        // Acabar juego
                        game.paused = false;
                        myHealthBar.kill();
                        this.exitToMenu();                        
                    }else if (choise == 1){ //reiniciar
                        game.paused = false;
                        // Primero finaliza
                        player.kill();
                        backSound.stop();
                        myHealthBar.kill();
                        // Y vuelve a empezar
                        game.state.start('level2');
                    }else if (choise == 2){ //resume
                        // Eliminar menu y etiqueta
                        menu.destroy();

                        // Unpause
                        game.paused = false;
                    }
                }
                else{
                    // Si pulsamos fuera del menu sale de la pausa
                    // Eliminar menu y etiqueta
                    menu.destroy();

                    // Unpause
                    game.paused = false;
                }
            }
        };        
        
        //PUNTOS (billetes)
        billetes = game.add.group();        
        billetes.enableBody = true;
         
        // 12 billetes iniciales
        for (var i = 0; i < 12; i++)
        {
            var x;
            var y;
            var lista;
            //pos aleatorias
            do { //encontrar coordenadas que no solapen con arbustos
                x = game.world.randomX;
                y = game.world.randomY;
                lista = arbustos.getTiles(x, y, 55, 29, true); //Tiles en torno al billete
                if (lista == false)
                    break;
            }while(lista[0].layer.name == 'Arbustos'); //mirar si alguno de los layers es arbustos
 
            //  Lo creamos
            var billete = billetes.create(x, y, 'billete');
            billete.animations.add('girar');
            billete.animations.play('girar', 2, true);
        }
        
        //Bidones de gasolina iniciales
        bidones = game.add.group();        
        bidones.enableBody = true;

        for (var i = 0; i < 8; i++)
        {
            var x;
            var y;
            var lista;
            //pos aleatorias
            do { //encontrar coordenadas que no solapen con arbustos
                x = game.world.randomX;
                y = game.world.randomY;
                lista = arbustos.getTiles(x, y, 55, 29, true); //Tiles en torno al billete
                if (lista == false)
                    break;
            }while(lista[0].layer.name == 'Arbustos'); //mirar si alguno de los layers es arbustos
 
            //  Lo creamos
            var bidon = bidones.create(x, y, 'bidon');
            bidon.anchor.set(0.5);
            bidon.scale.setTo(0.5, 0.5);
        }
        
        //SONIDOS
        pointSound = game.add.audio('point_sound');
        pointSound.volume -= 0.8;
        backSound = game.add.audio('back_music');
        backSound.volume -= 0.8;
        bombSound = game.add.audio('bomb_sound');
        gasSound = game.add.audio('blop');
        fireSound = game.add.audio('fire_sound');
        fireSound.volume += 0.8;
        timeSound = game.add.audio('time_sound');
        timeSound.volume += 0.8;
        iceSound = game.add.audio('ice_sound');
        shieldSound = game.add.audio('shield_sound');
        shieldSound.volume += 0.9;
        victorySound = game.add.audio('victory_sound');
        collideSound = game.add.audio('collision_sound');
        
        backSound.play();
        backSound.loopFull();
        
        //GRUPOS DE ICONOS DE BONUS 
        //Copos de nieve
        copos = game.add.group();        
        copos.enableBody = true;
        //Fuego
        llamas = game.add.group();
        llamas.enableBody = true;
        
        //ESCUDOS
        escudos = game.add.group();
        escudos.enableBody = true;
        
        //Tiempo extra
        tiempos_extra = game.add.group();
        tiempos_extra.enableBody = true;
        ///
        
        //Mostrar CONTADOR tiempo
        textoTiempo = game.add.bitmapText(0,0, 'fontTime', 'Tiempo restante: '+counter, 50);
        textoTiempo.position.x = game.global.worldWidth/2;
        textoTiempo.position.y = 40;
        textoTiempo.anchor.setTo(0.6, 0.6);
        textoTiempo.fixedToCamera = true;

        //Tween texto del tiempo
        var t = game.add.tween(textoTiempo.scale).to( {x: 0.6, y:0.6}, 1000, Phaser.Easing.Linear.None, true);
        t.onComplete.add(first, this);
        
        function first() {
            var t = game.add.tween(textoTiempo.scale).to( {x:0.7 , y:0.7}, 1000, Phaser.Easing.Linear.None, true);
            t.onComplete.add(next, this);
        }
        
        function next() {
           var t = game.add.tween(textoTiempo.scale).to( {x: 0.6, y:0.6}, 1000, Phaser.Easing.Linear.None, true);
            t.onComplete.add(first, this);
        }
        
        //Estado del nivel (puntos y gasoina)
        // creamos un grupo para los datos
        grupoPuntuacion = game.add.group();
        grupoPuntuacion.enableBody = true;
        //  Puntuacion actual        
        game.global.score = 0;
        //scoreText = game.add.text(60, 16, '0', { fontSize: '32px', fill: '#000' });
        scoreText = new Phaser.Text(game, 60, 16, '0', { fontSize: '32px', fill: '#000' });
        scoreText.fixedToCamera = true; //siempre se muestra
        scoreText.cameraOffset.setTo(70, 0); //arriba izq
        
        //var text = new Phaser.Text(game, 0, 0, "Text", {/*style object*/});group.add(text);

        icono_score = this.add.sprite(12, 5, 'billete',0);
        icono_score.scale.setTo(1.2, 1.2);
        icono_score.fixedToCamera = true;
        
        grupoPuntuacion.add(scoreText);
        grupoPuntuacion.add(icono_score);
        
        //Para la barra del NIVEL DE GASOLINA
        //Colores dependiendo del coche seleccionado
        bg_color = '#B40404';
        switch(game.global.car){
            case 'car':
                bar_color = '#0099ff';
                break;
            case 'car2':
                bar_color = '#00ff00';
                break;
            case 'car3':
                bar_color = '#ff6600';
                break;
            case 'car4':
                bar_color = '#ff6699';
                break;
            case 'car5':
                bar_color = '#cc0000';
                bg_color = '#ff9900';
                break;
            case 'car6':
                bar_color = '#ffffff';
                break;
            default:
                bar_color = '#ffff00';
        }
        
        //Creacion barra        
        var barConfig = {x: 180, y: 70, bg:{color:bg_color} , bar:{color:bar_color} , isFixedToCamera:true};        
        myHealthBar = new HealthBar(this.game, barConfig);
        //Empieza al 100%
        myHealthBar.setPercent(100);
        maxBar = 250; //en anchura de barra
        myHealthBar.barSprite._width = maxBar;
        
        var icono_gas = this.add.sprite(35, 70, 'bidon');
        icono_gas.anchor.set(0.5);
        icono_gas.scale.setTo(0.5, 0.5);
        icono_gas.fixedToCamera = true;
        
        grupoPuntuacion.add(icono_gas);
        grupoPuntuacion.add(myHealthBar.bgSprite);
        grupoPuntuacion.add(myHealthBar.barSprite);        
        
        //tambien añadimos el texto del tiempo al grupo
        grupoPuntuacion.add(textoTiempo);

        
        //TEMPORIZADORES
        //temporizador para bajar el nivel de gasolina
        timer_nivelGas = game.time.create(false); //false evita autodestruccion del temporizador
        timer_nivelGas.add(1000, this.bajarGas, this);      //baja cada segundo el nivel  
        timer_nivelGas.start();
        
        timer_invencible = game.time.create(false);
        timer_invencible.add(1000, this.actualizarInvencible, this);      //para invencibilidad por choque
        
        //timer del tiempo del bonus
        timer_bonus = game.time.create(false);
        //timer_bonus.start(); //aun no se llama
        
        //timer del parpadeo de invencibilidad
        timer_parpadeoPl = game.time.create(false);
        
        //temporizador para activar Parpadeo del contador los ultimos segundos
        timer_activarParpadeo = game.time.create(true);
        timer_activarParpadeo.add(Phaser.Timer.SECOND * (counter-5), this.activarParpadeo, this); //Empieza a parpadear cuando quedan 5 segundos
        timer_activarParpadeo.start();
        
        timer_activarParpadeoPl = game.time.create(false);
                
        //temporizador para crear los BONUS cada 10 segundos
        timer = game.time.create(false); //false evita autodestruccion del temporizador
        timer.add(TIEMPO_BONUS, this.createRandBonus, this);        
        timer.start();
        
        //Para disminuir el contador de tiempo
        game.time.events.loop(Phaser.Timer.SECOND, this.updateCounter, this);
        
        //temporizador FIN JUEGO
        timerEnd = game.time.create();        
        // Duracion: 1 minuto
        timerEnd.add(/*Phaser.Timer.MINUTE * 1*/ + Phaser.Timer.SECOND * counter, this.gameOver, this);        
        // iniciar cuenta
        timerEnd.start();
        
        //Temporizador revivir enemigo
        timer_revive = game.time.create(false);
        timer_revive.add(5000, this.crearEnemigos, this);        //lo vuelve a crear tras 5 secs
        timer_revive.start();
        
        
        //PARA GAMEPAD
        game.input.gamepad.start();
        pad1 = game.input.gamepad.pad1;
    },

    update: function() {
        //para las particulas del fuego que sigan al coche
        if (emitter.on==true){
            var px = player.body.velocity.x;
            var py = player.body.velocity.y;

            px *= -2;
            py *= -2;

            emitter.minParticleSpeed.set(px, py);
            emitter.maxParticleSpeed.set(px, py);

            emitter.emitX = player.x;
            emitter.emitY = player.y;
        }
        ////
        //para mantener los arboles y la puntuacion por encima de todo.
        game.world.bringToTop(tree); 
        game.world.bringToTop(grupoPuntuacion);

        //Para recoger bonificaciones y puntos
        game.physics.arcade.overlap(player, billetes, this.collectBonus, null, this);
        game.physics.arcade.overlap(player, copos, this.collectBonus, null, this);        
        game.physics.arcade.overlap(player, llamas, this.collectBonus, null, this);
        game.physics.arcade.overlap(player, escudos, this.collectBonus, null, this);
        game.physics.arcade.overlap(player, tiempos_extra, this.collectBonus, null, this);        
        
        //Para recoger la GASOLINA
        game.physics.arcade.overlap(player, bidones, this.collectBonus, null, this);
        
        //Comprobacion de si se ha quedado sin gasolina
        if (myHealthBar.barSprite._width == 0 && counter < 59){
            this.gameOver();
        }
        
        //Para que choque con el layer de los arbustos
        game.physics.arcade.collide(player, arbustos);
        
        //Para que choque con la fuente
        game.physics.arcade.collide(player, fuente);
                
        //Update de los enemigos. Comprobar cada uno de ellos
        enemigosVivos = 0;
        for (var i = 0; i < enemigos.length; i++){
            //Comprobar solo en los que estan vivos
            if (enemigos[i].enemigo.alive){
                enemigosVivos++;
                if(game.physics.arcade.collide(player, enemigos[i].enemigo)){
                    enemigos[i].colisionando = true;  //esta colisionando
                    //collideSound.play();
                    this.stopEnemigo(enemigos[i], player);  //cuando le chocamos le empujamos
                    
                    // para que el jugador parpadee cuando le chocan  si no es invencible
                    if (player.invencibleBonus == false){
                        timer_invencible.start();
                        timer_activarParpadeoPl.add(0, this.activarParpadeoPlayer, this); //Dura 1 segundo
                        timer_activarParpadeoPl.start();
                        this.bajarPts();
                    }                    
                }else if (enemigos[i].colisionando == false){ //solo update si no esta colisionando
                    if (flag_congelados == false){
                        game.physics.arcade.moveToObject(enemigos[i].enemigo, player, 200);
                    }else{
                        game.physics.arcade.moveToObject(enemigos[i].enemigo, player, 100);
                    }
                }
            
                for (var j = 0; j < enemigos.length; j++){
                    if (enemigos[j].enemigo.alive)
                        game.physics.arcade.collide(enemigos[j].enemigo, enemigos[i].enemigo);//se empujan entre ellos
                }
                //Choca con los arbustos
                if (game.physics.arcade.collide(enemigos[i].enemigo, arbustos)){
                    enemigos[i].colisionando = true;  //esta colisionando
                    this.stopEnemigo(enemigos[i]);
                }
                //Siempre mira hacia el jugador
                enemigos[i].enemigo.rotation = game.physics.arcade.angleBetween(enemigos[i].enemigo, player);
                //Para que los enemigos choquen con el emiter de fuego            
                game.physics.arcade.collide(enemigos[i].enemigo, emitter, this.killEnemy, null, this);
            }
        }
        
        if (game.physics.arcade.overlap(player, barrizales)){ //cuando pasa sobre barro
            this.movePlayerSlow();
        }else{ //mover normal
            this.movePlayer();
        }
        
        if (player.invencibleBonus == true){ //escudo solo por el bonus
            shield.alpha = 0.6;
            shield.x = player.x;
            shield.y = player.y;
        }else{
            shield.alpha = 0;
        }
        
        game.camera.follow(player);
        
        //Si se alcanzan los 120 puntos se acaba con victoria
        if(game.global.score == 120){
            this.gameOverWin();
        }
    },
    
    bajarPts: function(){
        if(player.invencible == false){
            if(game.global.score > 0){ //Cuando un enemigo choca al jugador, le resta 10 pts.
                game.global.score -= 10;
                scoreText.text = game.global.score;
            }
            scoreText.style.fill = '#FF0000';
            var textTween = game.add.tween(scoreText)
                textTween.to({
                    alpha: 0.5
                }, 20, Phaser.Easing.Cubic.None);
                textTween.to({
                    alpha: 1
                }, 20, Phaser.Easing.Cubic.None);
                textTween.to({
                    alpha: 0.5
                }, 20, Phaser.Easing.Cubic.None);
                textTween.to({
                    alpha: 1
                }, 20, Phaser.Easing.Cubic.None);
                textTween.start();
             scoreText.style.fill = '#000000';
        }
        player.invencible = true;
        timer_invencible.add(1000, this.actualizarInvencible, this); //durante 1 secs es invencible
    },
    
    actualizarInvencible: function(){
        player.invencible = false;
        player.alpha = 1;
        timer_parpadeoPl.stop(true);
    },
    
    crearEnemigos: function(){
        if(enemigosVivos < ENEMIGOS_MAX){
            // Busca al que falta y lo vuelve a revivir tras unos segundos
            for(var i = 0; i < enemigos.length; i++){
                if (!enemigos[i].enemigo.alive){
                    do { //para que no aparezca en un arbusto
                        var x = game.world.randomX;
                        var y = game.world.randomY;
                        lista = arbustos.getTiles(x, y, 170, 85, true); //Tiles en torno al coche enemigo
                        if (lista == false)
                            break; //salir del while
                    }while(lista[0].layer.name == 'Arbustos');
                    enemigos[i].enemigo.reset(x,y);
                    enemigosVivos++;
                    break; //salir del for
                }
            }
        }       
        timer_revive.add(5000, this.crearEnemigos, this);
    },
    
    activarParpadeo: function(){
        timer_textoTiempo = game.time.create(false);
        timer_textoTiempo.loop(250, this.parpadeoTiempo, this); //cada 250ms parpadea
        timer_textoTiempo.start();
    },
    
    parpadeoTiempo: function(){
        textoTiempo.visible = ! textoTiempo.visible; //si esta visible lo pasa a invisible y vv
    },
    
    activarParpadeoPlayer: function(){
        timer_parpadeoPl.loop(250, this.parpadeoPlayer, this); //cada 500ms parpadea
        timer_parpadeoPl.start();
        countParpadeo = 0;
    },
    
    parpadeoPlayer: function(){
        if (player.alpha == 0) 
            player.alpha = 1;
        else
            player.alpha = 0;

    },
    
    movePlayerSlow: function() {
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        player.body.angularVelocity = 0;
        
        // Si hay un pad conectado
        if (game.input.gamepad.supported && game.input.gamepad.active && pad1.connected){
            //indicator.animations.frame = 0;
            //Izq
            if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1){
                player.body.angularVelocity = -50;
            //Dcha
            }else if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1){
                player.body.angularVelocity = 50;
            }
            //siemrpre adelante
            player.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(player.angle, 200)); //siemrpe adelante
            
        //Con las flechas
        }else{
            if (cursors.left.isDown)
            {
                player.body.angularVelocity = -50;
            }
            else if (cursors.right.isDown)
            {
                player.body.angularVelocity = 50;
            }

            player.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(player.angle, 200)); //siemrpe adelante
        }
    },
    
    movePlayer: function() {
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        player.body.angularVelocity = 0;
        // Si hay un pad conectado
        if (game.input.gamepad.supported && game.input.gamepad.active && pad1.connected){
            //indicator.animations.frame = 0;
            //Izq
            if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1){
                player.body.angularVelocity = -200;
            //Dcha
            }else if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1){
                player.body.angularVelocity = 200;
            }
            //siemrpre adelante
            player.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(player.angle, 400)); //siemrpe adelante
            
        }else{
            if (cursors.left.isDown)
            {
                player.body.angularVelocity = -200;
            }
            else if (cursors.right.isDown)
            {
                player.body.angularVelocity = 200;
            }

            player.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(player.angle, 400)); //siemrpe adelante
        }
    },
    
    killEnemy: function(enemigo, emitter){
        enemigo.kill();
        enemigosVivos--;
        
        bombSound.play();
        
        emitterDestruction = explosions.getFirstExists(false);
        emitterDestruction.reset(enemigo.x, enemigo.y);
        emitterDestruction.play('kaboom', 30, false, true);        
    },
    
    collectBonus: function(player, bonus){
        bonus.kill();

        var tipo = bonus.key;
        
        switch(tipo){
            case 'icono-fuego':
                fireSound.play();
                timer_bonus.stop(true); //quita los eventos anteriores al coger nueva ventaja y true limpia el timer

                //activar emitter de fuego
                emitter.on=true;
                timer_bonus.start(); // Necesario para normal status
                //y se vuelve a poner el contador
                timer_bonus.add(5000, this.normalStatus, this, 'fuego');
                break;
            case 'copo':
                //congelar enemigos
                iceSound.play();
                timer_bonus.stop(true); //quita los eventos anteriores al coger nueva ventaja y true limpia el timer
                
                for (var i = 0; i < enemigos.length; i++){
                    enemigos[i].enemigo.loadTexture('enemigo_congelado', 0);
                }
                flag_congelados = true;
                timer_bonus.start(); // Necesario para normal status
                //y se vuelve a poner el contador
                timer_bonus.add(5000, this.normalStatus, this, 'copo');
                break;
            case 'bidon':
                gasSound.play();
                //Modificar barra gasolina. Por cada bidon cogido +10% gas
                gas_actual = myHealthBar.barSprite._width;
                if (gas_actual <= maxBar-20) //Si tiene mas no se puede sumar mas
                    myHealthBar.setWidth(gas_actual+20);
                else if (gas_actual > maxBar-20)
                    myHealthBar.setPercent(100); //maximo sin sobrepasarlo
                break;
            case 'billete':
                pointSound.play();
                game.global.score += 10;
                scoreText.text = game.global.score;  
                break;
            case 'shield_icon':
                shieldSound.play();
                timer_bonus.stop(true);
                //volver invencible (no bajan los puntos)
                player.invencibleBonus = true;
                timer_bonus.start(); // Necesario para normal status
                //y se vuelve a poner el contador
                timer_bonus.add(5000, this.normalStatus, this, 'fuego');
                break;
            case 'time_icon':
                timeSound.play();
                counter += 5;
                textoTiempo.setText('Tiempo restante: ' + counter);
                timerEnd.stop(true); //limpia timerEnd
                timerEnd.add(/*Phaser.Timer.MINUTE * 1*/ + Phaser.Timer.SECOND * counter, this.gameOver, this); 
                timerEnd.start();
                break;
        }        
    },
    
    normalStatus: function(tipo_bonus){
        if(emitter.on == true){
            emitter.on=false;
        }
        if(flag_congelados == true){
            for (var i = 0; i < enemigos.length; i++){
                enemigos[i].enemigo.loadTexture('enemigo', 0);
            }
            flag_congelados = false;
        }
        
        if(player.invencibleBonus == true){
            player.invencibleBonus = false;
        }
    },
    
    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    getRandomInt: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    
    /*Factoria creacion bonus*/
    createRandBonus: function(){
        //despues del tiempo creara un bonus aleatorio        
        var x = 0;
        var y = 0;
        
        var n = this.getRandomInt(0,5); //para elegir qué crear. Los billetes salen mas
        x_separacion = 90; y_separacion = 90;
        //pos aleatorias
        do { //encontrar coordenadas que no solapen con arbustos
            x = game.world.randomX;
            y = game.world.randomY;
            var lista = arbustos.getTiles(x, y, x_separacion, y_separacion, true); //Tiles en torno al bonus
            if (lista == false)
                break;
        }while(lista[0].layer.name == 'Arbustos');
        
        /*
        billetes: 5 al 7;  fuegos: 0;   copos: 1;   gas: 2   escudos: 3    ;tiempo extra: 4
        */
        switch(n){
            case 0:
                //Crear bonus fuego
                var fuego = llamas.create(x, y, 'icono-fuego');
                fuego.scale.setTo(0.1, 0.1);
                break;
            case 1:
                var copo = copos.create(x, y, 'copo');
                break;
            case 2:
                var bidon = bidones.create(x, y, 'bidon');
                bidon.anchor.set(0.5);
                bidon.scale.setTo(0.5, 0.5);
                break;
            case 3:
                var escudo = escudos.create(x, y, 'shield_icon');
                escudo.scale.setTo(0.4, 0.4);       
                break;
            case 4:
                var tiempo_extra = tiempos_extra.create(x, y, 'time_icon');
                tiempo_extra.scale.setTo(0.8, 0.8);       
                break;
            default:
                //billetes
                var billete = billetes.create(x, y, 'billete');

                billete.animations.add('girar');
                billete.animations.play('girar', 2, true);        
        }

        //y se vuelve a poner el contador de creacion
        timer.add(TIEMPO_BONUS, this.createRandBonus, this);
    },
    
    bajarGas: function(){
        //Se reduce el nivel de gasolina
        gas_actual = myHealthBar.barSprite._width;
        myHealthBar.setWidth(gas_actual-5);
        
         //y se vuelve a poner el contador
        timer_nivelGas.add(1000, this.bajarGas, this);
    },
    
    //Funcion auxiliar para choque entre jugador y enemigo
    stopEnemigo: function(enemigoI, player){

        enemigoI.enemigo.body.velocity.set(-100);
        
        game.physics.arcade.velocityFromRotation(enemigoI.enemigo.rotation, -100, enemigoI.enemigo.body.velocity);
        
        game.time.events.add(1000, this.goEnemigo, this, enemigoI); //para volver a moverse
    },
    
    goEnemigo: function(enemigoI){
        enemigoI.colisionando = false;
    },
    
    exitToMenu:function(){
        player.kill();
        backSound.stop();
        myHealthBar.kill();
        //llama al menu
        game.state.start('menu');
    }, 
    
    gameOver: function() {
        flag_gameOver = true;
        spacebar = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        endScoreLabel = game.add.bitmapText(0,0, 'fontGameOver_score', 'Puntutacion: '+game.global.score, 40);
        endScoreLabel.align='center';
        endScoreLabel.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2 - 170) / this.game.camera.scale.y;
        endScoreLabel.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2) / this.game.camera.scale.x;
        endScoreLabel.anchor.setTo(0.6, 0.6);        
        
        endLabel = game.add.bitmapText(0,0, 'fontGameOver_score', 'GAME OVER', 60);
        endLabel.align='center';
        endLabel.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2 - 120) / this.game.camera.scale.y;
        endLabel.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2) / this.game.camera.scale.x;
        endLabel.anchor.setTo(0.6, 0.6);
        
        endScoreLabel = game.add.bitmapText(0,0, 'fontGameOver_score', 'Pulsa SPACE para volver al menu', 40);
        endScoreLabel.align='center';
        endScoreLabel.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2 - 70) / this.game.camera.scale.y;
        endScoreLabel.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2 + 30) / this.game.camera.scale.x;
        endScoreLabel.anchor.setTo(0.6, 0.6);     
        
        
        //Actualizar starsArray
        //Vamos a poner que para 3 estrellas haya que coger todas las estrellas(12)
        //para 2 estrellas coger 10
        //para 1 coger 6
        if (game.global.score == 120 && game.global.starsArray[game.global.level-1] <= 3){
            game.global.starsArray[game.global.level-1] = 3;
        }else if(game.global.score >= 100 && game.global.starsArray[game.global.level-1] <= 2){
            game.global.starsArray[game.global.level-1] = 2;
        }else if (game.global.score >= 60 && game.global.starsArray[game.global.level-1] <= 1){
            game.global.starsArray[game.global.level-1] = 1;
        }else
            game.global.starsArray[game.global.level-1] = 0;
        
         // Si completamos un nivel y el siguiente esta bloqueado - y existe - lo desbloqueamos
		if(game.global.starsArray[game.global.level-1]>0 && game.global.starsArray[game.global.level]==4 && game.global.level<game.global.starsArray.length){
			game.global.starsArray[game.global.level] = 0;
        }
        
        //Al pulsar espacio acaba
        spacebar.onDown.add(function () {
            // Fin juego        
            player.kill();
            backSound.stop();
            spacebar = null;
            //llama al menu
            game.state.start('menu');
            game.paused = false;
        });
        game.paused = true;
    },
    
    //Solo cuando ha acabado cogiendo los 120 puntos
    gameOverWin: function() {
        flag_gameOver = true;
        victorySound.play();
        
        spacebar = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        endScoreLabel = game.add.bitmapText(0,0, 'fontGameOver_score', 'Puntutacion: '+game.global.score, 40);
        endScoreLabel.align='center';
        endScoreLabel.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2 - 170) / this.game.camera.scale.y;
        endScoreLabel.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2) / this.game.camera.scale.x;
        endScoreLabel.anchor.setTo(0.6, 0.6);   
        
        endLabel = game.add.bitmapText(0,0, 'fontGameOver_score', 'VICTORIA!!', 60);
        endLabel.align='center';
        endLabel.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2 - 120) / this.game.camera.scale.y;
        endLabel.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2) / this.game.camera.scale.x;
        endLabel.anchor.setTo(0.6, 0.6);
        
        endScoreLabel = game.add.bitmapText(0,0, 'fontGameOver_score', 'Pulsa SPACE para volver al menu', 40);
        endScoreLabel.align='center';
        endScoreLabel.position.y = (this.game.camera.view.y + this.game.global.worldHeight/2 - 70) / this.game.camera.scale.y;
        endScoreLabel.position.x = (this.game.camera.view.x + this.game.global.worldWidth/2 + 30) / this.game.camera.scale.x;
        endScoreLabel.anchor.setTo(0.6, 0.6);     
        
        //Actualizar starsArray con 3 estrellas
        game.global.starsArray[game.global.level-1] = 3;
        
        // Si completamos un nivel y el siguiente esta bloqueado - y existe - lo desbloqueamos
		if(game.global.starsArray[game.global.level-1]>0 && game.global.starsArray[game.global.level]==4 && game.global.level<game.global.starsArray.length){
			game.global.starsArray[game.global.level] = 0;
        }
        
        //Al pulsar espacio acaba
        spacebar.onDown.add(function () {
            // Fin juego        
            player.kill();
            backSound.stop();
            spacebar = null;
            //llama al menu
            game.state.start('menu');
            game.paused = false;
        });
        game.paused = true
    },
    
    updateCounter: function() {
        if (!game.physics.arcade.isPaused)
            counter--;

        textoTiempo.setText('Tiempo restante: ' + counter);
    },
  
    render: function(){
        //game.debug.quadTree(game.physics.arcade.quadTree);
        //game.debug.bodyInfo(player, 32, 32);
        //game.debug.body(player);
        //game.debug.cameraInfo(game.camera, 32, 32);
        //game.debug.spriteCoords(player, 32, 500);
        /*emitter.forEachAlive(function(particle) {
            game.debug.body(particle,'red',false);
            game.debug.spriteBounds(particle, 'pink',false);
        });*/        
        
          /*  game.debug.inputInfo(32, 32);
    //game.debug.spriteInputInfo(sprite, 32, 130);
    game.debug.pointer( game.input.activePointer );*/
    }
};